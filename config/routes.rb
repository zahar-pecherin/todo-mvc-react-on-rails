Rails.application.routes.draw do
  root 'todo#index'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  resources :items, only: [:index, :create, :update, :destroy] do
    member do
      put :change_status
    end
    collection do
      put :clear_completed
      put :toggle_all
    end
  end
end
