# frozen_string_literal: true

class ItemsController < ApplicationController
  before_action :set_item, only: [:destroy, :update, :change_status]

  def index
    @items = Item.all.order(created_at: :desc)
    render json: {items: @items}
  end

  def create
    @item = Item.new(item_params)
    if @item.save
      render json: @item
    else
      render json: @item.errors, status: :unprocessable_entity
    end
  end

  def update
    if @item.update(item_params)
      render json: @item
    else
      render json: @item.errors, status: :unprocessable_entity
    end
  end

  def destroy
    @item.destroy
    head :no_content
  end

  def change_status
    @item.update(active: !@item.active)
    head :no_content
  end

  def clear_completed
    Item.where(active: false).destroy_all
    render json: {items: Item.all.order(created_at: :desc)}
  end

  def toggle_all
    params[:count] > 0 ?  Item.where(active: true).update(active: false) : Item.where(active: false).update(active: true)
    render json: {items: Item.all.order(created_at: :desc)}
  end

  private

  def set_item
    @item = Item.find(params[:id])
  end

  def item_params
    params.require(:item).permit(:description)
  end
end