import { combineReducers } from 'redux';

// Reducers
import itemReducer from './item-reducer';

var reducers = combineReducers({
  itemState: itemReducer
});

export default reducers;