import * as types from '../actions/action-types';
import _ from 'lodash';

const initialState = {
  items: [],
  description: '',
  loading: false,
  shown: 'all'
};

const itemReducer = function(state = initialState, action) {


  switch(action.type) {
    case types.GET_ITEMS_SUCCESS:
      var actionItems = action.items.map(item => Object.assign({}, item, {edit: false, params: item.description}));
      return Object.assign({}, state, { items: actionItems});

    case types.CHANGE_DESCRIPTION:
      return Object.assign({}, state, { description: action.description});

    case types.ADD_ITEM_SUCCESS:
      const withinItems = _.union([action.item], state.items);
      return Object.assign({}, state, { items: withinItems, description: '' });

    case types.DELETE_ITEM_SUCCESS:
      const newItems = _.filter(state.items, item => item.id != action.itemId);
      return Object.assign({}, state, { items: newItems });

    case types.CHANGE_STATUS:
      return {...state,
        items: state.items.map(item => item.id === action.itemId ?
      { ...item, active: !item.active } : item
      )
    }

    case types.CHANGE_FILTER_PARAMS:
      return Object.assign({}, state, { shown: action.param});

    case types.TOGGLE_ALL_SUCCES:
      var items_toggle = action.items.map(item => Object.assign({}, item, {edit: false, params: item.description}));
      return Object.assign({}, state, { items: items_toggle});

    case types.CLEAR_COMPLETED_SUCCESS:
      var items_clear = action.items.map(item => Object.assign({}, item, {edit: false, params: item.description}));
      return Object.assign({}, state, { items: items_clear});

    case types.EDIT_ITEM:
      return {...state,
        items: state.items.map(item => item.id === action.itemId ?
        { ...item, active: item.active, edit: !item.edit } : item
      )
    }

    case types.CHANGE_ITEM_DESCRIPTION:
      return {...state,
        items: state.items.map(item => item.id === action.itemId ?
        { ...item, description: action.params } : item
      )
    }

    case types.CANCEL_CHANGE_DESCRIPTION:
      return {...state,
        items: state.items.map(item => item.id === action.itemId ?
        { ...item, description: item.params, edit: !item.edit } : item
      )
    }

    case types.UPDATE_ITEM_SUCCESS:
      return {...state,
        items: state.items.map(item => item.id === action.item.id ?
        { ...item, description: action.item.description, edit: !item.edit, params: action.item.description } : item
      )
    }
}

  return state;
};

export default itemReducer;