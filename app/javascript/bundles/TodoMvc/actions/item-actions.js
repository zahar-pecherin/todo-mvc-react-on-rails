import * as types from '../actions/action-types';
import axios from 'axios';
import { push } from 'react-router-redux'

export function getItemsSuccess(items) {
  return {
    type: types.GET_ITEMS_SUCCESS,
    items
  };
}

export function addItemSuccess(item) {
  return {
    type: types.ADD_ITEM_SUCCESS,
    item
  };
}

export function deleteItemSuccess(itemId) {
  return {
    type: types.DELETE_ITEM_SUCCESS,
    itemId
  };
}

export function updateItemSuccess(item) {
  return {
    type: types.UPDATE_ITEM_SUCCESS,
    item
  };
}

export function editItem(itemId) {
  return {
    type: types.EDIT_ITEM,
    itemId
  };
}

export function changeDescription(description) {
  return {
    type: types.CHANGE_DESCRIPTION,
    description
  };
}

export function changeStatusSuccess(itemId) {
  return {
    type: types.CHANGE_STATUS,
    itemId
  };
}

export function changeFilterParams(param) {
  return {
    type: types.CHANGE_FILTER_PARAMS,
    param
  };
}

export function toggleAllSuccess(items) {
  return {
    type: types.TOGGLE_ALL_SUCCES,
    items
  };
}

export function clearCompletedSuccess(items) {
  return {
    type: types.CLEAR_COMPLETED_SUCCESS,
    items
  };
}

export function changeItemDescription(itemId, params) {
  return {
    type: types.CHANGE_ITEM_DESCRIPTION,
    itemId,
    params
  };
}

export function cancelChangeDescription(itemId) {
  return {
    type: types.CANCEL_CHANGE_DESCRIPTION,
    itemId,
  };
}

export function requestToLoadItems(_this) {
  return (dispatch) => {
    axios.get('/items')
      .then(function(response){
        dispatch(getItemsSuccess(response.data.items));
        return response;
      })
      .catch(function(error){
        console.log(error)
      });
  };
}

export function requestToAddItem(params) {
  return (dispatch) => {
    axios.post('/items', {item: {description: params} })
      .then(function (response) {
        dispatch(addItemSuccess(response.data));
        return response;
      })
      .catch(function (error) {
        console.log(error)
      });
  };
}

export function requestToDeleteItem(itemID) {
  return (dispatch) => {
    axios.delete('/items/' + itemID)
      .then(function (response) {
        return dispatch(deleteItemSuccess(itemID));
      })
      .catch(function (error) {
        console.log(error);
      });
  };
}

export function requestToUpdateItem(itemID, params) {
  return (dispatch) => {
    axios.put('/items/' + itemID, {item: {description: params} })
      .then(function (response) {
        return dispatch(updateItemSuccess(response.data));
      })
      .catch(function (error) {
        console.log(error);
      });
  };
}

export function requestToChangeStatus(itemID) {
  return (dispatch) => {
    axios.put('/items/' + itemID + '/change_status')
      .then(function (response) {
        return dispatch(changeStatusSuccess(itemID));
      })
      .catch(function (error) {
        console.log(error);
      });
  };
}

export function requestToToggleAll(count) {
  return (dispatch) => {
    axios.put('/items/toggle_all', {count: count})
      .then(function(response){
        dispatch(toggleAllSuccess(response.data.items));
        return response;
      })
      .catch(function(error){
        console.log(error)
      });
  };
}

export function requestToClearCompleted() {
  return (dispatch) => {
    axios.put('/items/clear_completed')
      .then(function(response){
        dispatch(clearCompletedSuccess(response.data.items));
        return response;
      })
      .catch(function(error){
        console.log(error)
      });
  };
}
