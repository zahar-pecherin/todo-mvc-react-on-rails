import React from 'react';
import createReactClass from 'create-react-class';
import store from '../store';
import { changeFilterParams } from '../actions/item-actions';

const TodoFooter = createReactClass ({

  setFilterParams(params) {
    store.dispatch(changeFilterParams(params))
  },

  pluralize(count, word){
    return count === 1 ? word : word + 's';
  },

  render() {
    let clearButton = null;
    if (this.props.completedCount > 0) {
      clearButton = (
        <button
          className="clear-completed"
          onClick={this.props.onClearCompleted}>
          Clear completed
        </button>
      );
    }

    return (
      <footer className="footer">
              <span className="todo-count">
                <strong>{this.props.count}</strong> {this.pluralize(this.props.count, 'item')} left
              </span>
        <ul className="filters">
          <li>
            <span
              className={this.props.shown === 'all' ? 'selected' : ''}
              onClick={this.setFilterParams.bind(null, 'all')}
            >
              All
            </span>
          </li>
          {' '}
          <li>
            <span
              className={this.props.shown === 'active' ? 'selected' : ''}
              onClick={this.setFilterParams.bind(null, 'active')}>
              Active
            </span>
          </li>
          {' '}
          <li>
            <span
              className={this.props.shown === 'completed' ? 'selected' : ''}
              onClick={this.setFilterParams.bind(null, 'completed')}>
              Completed
            </span>
          </li>
        </ul>
        {clearButton}
      </footer>
    )
  }
});

export default TodoFooter;
