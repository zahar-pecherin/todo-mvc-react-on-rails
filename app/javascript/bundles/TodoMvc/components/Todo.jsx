import React from 'react';
import { Provider } from 'react-redux';
import createReactClass from 'create-react-class';
import store from '../store';
import TodoList from './TodoList';

const Todo = createReactClass ({
  render() {
    return (
      <Provider store={store}>
        <section className="todoapp">
          <TodoList items={this.props.items}/>
        </section>
      </Provider>
    )
  }
});

export default Todo;
