import React from 'react';
import createReactClass from 'create-react-class';
import { connect } from 'react-redux';
import store from '../store';
import { requestToAddItem, changeDescription } from '../actions/item-actions';

const TodoComposer = createReactClass ({
  onChange(event) {
    store.dispatch(changeDescription(event.target.value))
  },

  onKeyDown(event) {
    if (event.keyCode === 13) {
      event.preventDefault();
      this.createItem();
    }
  },

  createItem() {
    let description = this.props.description;
    if (description) {
      this.props.requestToAdd(this.props.description);
    }
  },

  render() {
    return (
      <div>
        <header className="header">
          <input ref="newField"
                 className='new-todo'
                 type='text'
                 placeholder='What needs to be done?'
                 onKeyDown={ this.onKeyDown }
                 onChange={ this.onChange }
                 value={ this.props.description }
                 autoFocus={true}/>
        </header>
      </div>
    )
  }
});

const mapStateToProps = function(store) {
  return {
    description: store.itemState.description,
  };
};

const mapDispatchToProps = function(dispatch) {
  return {
    requestToAdd: function(params) {dispatch(requestToAddItem(params))},
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(TodoComposer)
