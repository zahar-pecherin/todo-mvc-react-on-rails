import React from 'react';
import createReactClass from 'create-react-class';
import { connect } from 'react-redux';
import { requestToDeleteItem, requestToUpdateItem, requestToChangeStatus, editItem, changeItemDescription, cancelChangeDescription } from '../actions/item-actions';

const TodoItem = createReactClass ({
  getInitialState: function () {
    return {editText: this.props.item.description};
  },

  handleDelete() {
    this.props.requestToDelete(this.props.item.id);
  },

  handleEdit() {
    this.props.editItem(this.props.item.id)
  },

  changeStatus() {
    this.props.requestToChange(this.props.item.id);
  },

  handleChange(event) {
    this.props.changeItemDescription(this.props.item.id, event.target.value)
  },

  handleKeyDown(event) {
    if (event.which === 27) {
      this.props.cancelChangeDescription(this.props.item.id);
    } else if (event.which === 13) {
      this.handleSubmit(event);
    }
  },

  handleSubmit(event) {
    if (event.target.value) {
      this.props.requestToUpdate(this.props.item.id, event.target.value)
    }
  },

  render() {
    let editRow = <input
      autoFocus
      ref="editField"
      className="edit"
      value={this.props.item.description}
      onBlur={this.handleSubmit}
      onChange={this.handleChange}
      onKeyDown={this.handleKeyDown}
    />;

    let description = <label onDoubleClick={this.handleEdit}>
      {this.props.item.description}
    </label>

    let itemDescription = this.props.item.edit ? editRow : description;

    return (
      <li className={this.props.item.active ? '' : 'completed'}>
        <div className="view">
          <input
            className="toggle"
            type="checkbox"
            checked={!this.props.item.active}
            onChange={this.changeStatus}
          />
          {itemDescription}
          <button className="destroy" onClick={this.handleDelete} />
        </div>
      </li>
    )
  }
});

const mapStateToProps = function(store) {
  return {
    items: store.itemState.items,
  };
};

const mapDispatchToProps = function(dispatch) {
  return {
    requestToDelete: function(item_id) {dispatch(requestToDeleteItem(item_id))},
    requestToUpdate: function(item_id, params) {dispatch(requestToUpdateItem(item_id, params))},
    requestToChange: function(item_id) {dispatch(requestToChangeStatus(item_id))},
    editItem: function(item_id) {dispatch(editItem(item_id))},
    changeItemDescription: function(item_id, params) {dispatch(changeItemDescription(item_id, params))},
    cancelChangeDescription: function(item_id) {dispatch(cancelChangeDescription(item_id))},
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(TodoItem)
