import React from 'react';
import createReactClass from 'create-react-class';
import { connect } from 'react-redux';
import { requestToLoadItems, requestToToggleAll, requestToClearCompleted} from '../actions/item-actions';

import TodoItem from './TodoItem';
import TodoComposer from './TodoComposer';
import TodoFooter from './TodoFooter';

const TodoList = createReactClass ({
  componentDidMount() {
    this.props.requestToLoad(this);
  },

  toggleAll(count) {
    this.props.requestToToggle(count);
  },

  clearCompleted() {
    this.props.requestToClear();
  },

  render() {
    let footer = null;
    let main = null;
    let items = this.props.items;
    let activeItemsCount = items.filter(function (item) { return item.active }).length;
    let completedCount = items.length - activeItemsCount;

    let shownItems = items.filter(function (item) {
      switch (this.props.shown) {
        case 'active':
          return item.active;
        case 'completed':
          return !item.active;
        case 'all':
          return true;
      }
    }, this);

    let todoItems = shownItems.map(function (item) {
      return (
        <TodoItem item={item}
                  key={item.id}/>
      );
    }, this);

    if (items.length) {
      main = (
        <section className="main">
          <input
            className="toggle-all"
            id="toggle-all"
            type="checkbox"
            onChange={this.toggleAll.bind(null, activeItemsCount)}
            checked={activeItemsCount === 0}
          />
          <label
            htmlFor="toggle-all"
          />
          <ul className="todo-list">
            {todoItems}
          </ul>
        </section>
      );
    }

    if (activeItemsCount || completedCount) {
      footer =
        <TodoFooter
          count={activeItemsCount}
          completedCount={completedCount}
          shown={this.props.shown}
          onClearCompleted={this.clearCompleted}
        />;
    }

    return (
      <div className='todo-list'>
        <h1>ToDo MVC</h1>
        <TodoComposer />
        {main}
        {footer}
      </div>
    )
  }
});

const mapStateToProps = function(store) {
  return {
    items: store.itemState.items,
    shown: store.itemState.shown,
  };
};

const mapDispatchToProps = function(dispatch) {
  return {
    requestToLoad: function(_this) {dispatch(requestToLoadItems(_this))},
    requestToToggle: function(count) {dispatch(requestToToggleAll(count))},
    requestToClear: function() {dispatch(requestToClearCompleted())},
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(TodoList);
