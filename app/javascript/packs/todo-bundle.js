import ReactOnRails from 'react-on-rails';

import Todo from '../bundles/TodoMvc/components/Todo';

// This is how react_on_rails can see the HelloWorld in the browser.
ReactOnRails.register({
  Todo,
});
